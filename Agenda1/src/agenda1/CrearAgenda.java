/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda1;

import java.io.File;
import javax.swing.JOptionPane;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author alumno
 */
public class CrearAgenda {

    public static void main(String[] args) {
        final int SI = 0;
        final int NO = 1;
        String fileName = "agenda.txt";
        File fichero = new File(fileName);
        BufferedWriter agenda;
        try {
            
            if (fichero.exists()) {
                int sino = JOptionPane.showConfirmDialog(null, "El fichero ya existe, ¿Desea sobreescribirlo?", "Fichero existe", JOptionPane.YES_NO_OPTION);
                if (sino == SI) {
                    agenda = new BufferedWriter(new FileWriter(fichero));
                }else{
                    //añadir entradas al final
                    agenda = new BufferedWriter(new FileWriter(fichero,true));
                }

            } else {
                //crear el fichero y añadir entradas
                agenda = new BufferedWriter(new FileWriter(fichero));
            }
            
            agenda.write("IES Galileo");
            agenda.newLine();
            agenda.write("c/ Villabañez s/n");
            agenda.newLine();
            agenda.write("983205640");
            agenda.newLine();
            
            agenda.write("María Fernández");
            agenda.newLine();
            agenda.write("c/ Gamazo 25, 3ºA");
            agenda.newLine();
            agenda.write("983102030");
            agenda.newLine();
            
            agenda.close();
            
        } catch (IOException e) {

        }
    }
}
